package com.example.springboot.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.springboot.dto.ReqTestCIMBQ1;
import com.example.springboot.dto.ReqTestCIMBQ2;
import com.example.springboot.dto.ReqTestCIMBQ3;

@Service
public class TestCIMBService {
	
	public Map<String, Object> Q1(ReqTestCIMBQ1 dto) {
		Map<String, Object> map = new HashMap<>();
		map.put("status", HttpStatus.BAD_REQUEST);
		
		if(dto.getPrices() == null || dto.getDayBuy() == null || dto.getDaySell() == null) {
			map.put("message", "prices or dayBuy or daySell is required.");
	        return map;
		} else if(
				dto.getDayBuy() >= dto.getDaySell() 
				|| dto.getDayBuy() > dto.getPrices().size()
				|| dto.getDaySell() > dto.getPrices().size()
				|| dto.getDayBuy() < 1
				|| dto.getDaySell() < 1
				|| dto.getDayBuy() <= 0
				|| dto.getDaySell() <= 0) {
			map.put("message", "Not allowed! please check input.");
	        return map;
		} else {
			boolean constraints1 = dto.getPrices().size() > (10^5) || dto.getPrices().size() == 0;
			boolean constraints2 = dto.getPrices().stream().filter(prices -> prices > (10^4) || prices < 0).findFirst().isPresent();
			if(constraints1 || constraints2) {
				map.put("message", "Constraints error : 1 <= prices.length <= 10^5 or 0 <= prices[i] <= 10^4.");
		        return map;
			}
		}
		
		Integer buy = dto.getPrices().get(dto.getDayBuy() - 1);
		Integer sell = dto.getPrices().get(dto.getDaySell() - 1);
		Integer calculate = sell - buy;
		Integer data = 0;
		
		if(calculate > 0) data = calculate;
		
		map.put("message", "Success.");
		map.put("status", HttpStatus.OK);
        map.put("result", data);
		
		return map;
	}
	
	public Map<String, Object> Q2(ReqTestCIMBQ2 dto) {
		
		Map<String, Object> map = new HashMap<>();
		map.put("status", HttpStatus.BAD_REQUEST);
		
		String version1 = dto.getVersion1();
		String version2 = dto.getVersion2();
		
		Integer data = 0;
		if (version1 == null || version2 == null) {
			map.put("message", "version1 and version2 is required.");
	        return map;
		} else if (!version1.replaceAll("\\.", "").matches("[0-9]+") || !version2.replaceAll("\\.", "").matches("[0-9]+")) {
			map.put("message", "Constraints error : version1 and version2 only contain digits and '.'.");
	        return map;
		} else if (version1.charAt(0) == '0' || version2.charAt(0) == '0') {
			map.put("message", "Constraints error : version1 and version2 are valid version numbers.");
	        return map;
		} else if (version1.length() == 0 || version2.length() == 0 || version2.length() > 500) {
			map.put("message", "Constraints error : 1 <= version1.length, version2.length <= 500.");
	        return map;
		} else if (version1.replaceAll("\\.", "").length() > 10 || version1.replaceAll("\\.", "").length() > 10 ) {
			map.put("message", "Constraints error : All the given revisions in version1 and version2 can be stored in a **32-bit integer**.");
	        return map;
		}
		
		String[] arrVer1 = version1.split("\\.");
		String[] arrVer2 = version2.split("\\.");
		
		for(int index = 0; index < 10; index++) {
			int longV1 = 0;
			int longV2 = 0;
			if (index < arrVer1.length) {
				longV1 = Integer.parseInt(arrVer1[index]);
			}
			if (index < arrVer2.length) {
				longV2 = Integer.parseInt(arrVer2[index]);
			}
			if(longV1 > longV2) {
				data = 1;
				break;
			} else if(longV1 < longV2) {
				data = -1;
				break;
			} else if (index >= (arrVer1.length - 1) && index >= (arrVer2.length - 1)) {
				break;
			}
		}
		
		map.put("message", "Success.");
		map.put("status", HttpStatus.OK);
        map.put("result", data);
		
		return map;
	}
	
	public Map<String, Object> Q3(ReqTestCIMBQ3 dto) {
		Map<String, Object> map = new HashMap<>();
		map.put("status", HttpStatus.BAD_REQUEST);
		
		Integer n = dto.getN();
		if (n == null) {
			map.put("message", "n is required.");
	        return map;
		} if (n < 1 || n > 45) {
			map.put("message", "Constraints error : 1 <= n <= 45.");
	        return map;
		}
		
		map.put("message", "Success.");
		map.put("status", HttpStatus.OK);
        map.put("result", n <= 2 ? n : (n-1) + (n-2));
		
		return map;
	}
}
