package com.example.springboot.dto;

import lombok.Data;

@Data
public class ReqTestCIMBQ2 {
	private String version1;
	private String version2;
}
