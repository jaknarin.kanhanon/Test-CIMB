package com.example.springboot.dto;

import java.util.List;

import lombok.Data;

@Data
public class ReqTestCIMBQ1 {
	private List<Integer> prices;
	private Integer dayBuy;
	private Integer daySell;
}
