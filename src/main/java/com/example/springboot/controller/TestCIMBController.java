package com.example.springboot.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.springboot.dto.ReqTestCIMBQ1;
import com.example.springboot.dto.ReqTestCIMBQ2;
import com.example.springboot.dto.ReqTestCIMBQ3;
import com.example.springboot.service.TestCIMBService;

@RestController
public class TestCIMBController {
	
	@Autowired
	private TestCIMBService testCIMBService;

	/*
		curl --location 'http://localhost:8080/Q1' \
		--header 'Content-Type: application/json' \
		--data '{
		    "prices" : [7,1,5,3,6,7],
		    "dayBuy" : 2,
		    "daySell" : 5
		}'
	*/
	@PostMapping("/Q1")
	public Map<String, Object> testQ1(@RequestBody ReqTestCIMBQ1 dto) throws Exception {
		return this.testCIMBService.Q1(dto);
	}
	
	/*
		curl --location 'http://localhost:8080/Q2' \
		--header 'Content-Type: application/json' \
		--data '{
		    "version1" : "1.100.01",
		    "version2" : "1.101.02"
		}'
	*/
	@PostMapping("/Q2")
	public Map<String, Object> testQ2(@RequestBody ReqTestCIMBQ2 dto) throws Exception {
		return this.testCIMBService.Q2(dto);
	}
	
	/*
		curl --location 'http://localhost:8080/Q3' \
		--header 'Content-Type: application/json' \
		--data '{
		    "n" : 3
		}'
	*/
	@PostMapping("/Q3")
	public Map<String, Object> testQ3(@RequestBody ReqTestCIMBQ3 dto) throws Exception {
		return this.testCIMBService.Q3(dto);
	}

}
